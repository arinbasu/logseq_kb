- There can be page tags as well
- But #zettelkasten will work just fine
- ## What is zettelkasten?
  Zettelkasten is a german word meaning slip box where individual atomised information in slip boxes are stored and interlinked. This was used by [[Niklas Luhmann]],  a german sociologist to write papers and manage personal information
- Each note in zettelkasten must have the five properties
- 1. They must be atomic
  2. They must have an ID assigned to them
  3. They must have at least backlink to note where they were forked from
  4. They may have additonal forward links to show where they are linked to
  5. They must have tags
- Implementing an exact zettelkasten in digital world is somewhat cumbersome. 
  For example, instead of zettels, we have blocks. Each block has its own identification, but it is not easy to find them but also not necessary. For example, if I use logseq as #zettelkasten then, logseq will assign a block address to this block and will reference it correctly in any page that arises from THIS block.
- If you click on [[Niklas Luhmann]] page, you will see that it BACKLINKS to two blocks. THIS and the "What is zettelkasten" blocks. It will also reference to this page. So, backlink is already taken care of
- In terms of tags, you have two ways to add tags: (1) page tags as page properties, or (2) hashtags with a hash mark and small letter as the first letter to indicate the tag. I prefer the second one as this gives you an automatic page that lists all referencing pages to the tag which in turn gets its own page.
- Each block is an #atomic information source. So, for the tag "atomic", this PAGE and this BLOCK nested within THIS page are referenced.
- Three different types of #zettel are seen:
  * Fleeting zettels or notes: these are the ones where you keep transient thoughts
  * Literature zettels: these usually stay within your literature or bibliography managers such as paperpile
  * Permanent zettels: these are the blocks. Like THIS block is a permanent zettel about types of zettels, but you will see that this block is referenced within this page in the #zettel page
-