- Diet, Physical Activity, and Screen Time among School
  Students in Manipur
  Markordor Lyngdoh, Brogen Singh Akoijam, S. Agui RK, Kh Sonarjit Singh
  Department of Community Medicine, Regional Institute of Medical Sciences, Imphal, Manipur, India
- https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6625269/pdf/IJCM-44-134.pdf
	- 929 children, cross-sectional survey
	- Manipur
	- Questionnaires:
		- Saint‑Maurice PF, Welk GJ. Web‑based assessments of physical activity
		  in youth: Considerations for design and scale calibration. J Med Internet
		  Res 2014;16:e269
		- Kumar S, Ray S, Roy D, Ganguly K, Dutta S, Mahapatra T, et al.
		  Exercise and eating habits among urban adolescents: A cross‑sectional
		  study in Kolkata, India. BMC Public Health 2017;17:468.
		- https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5437535/pdf/12889_2017_Article_4390.pdf
		- (This paper has several references on dietary questionnaires)
- Report card, https://www.activehealthykids.org/wp-content/uploads/2018/11/india-report-card-long-form-2018.pdf
- https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8543165/pdf/f1000research-10-77935.pdf
- Search results:
	- https://dsearch.com/search?q=diet%20physical%20activity%20india%20adolescents%20cse
	-