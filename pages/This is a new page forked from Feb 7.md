- You can see that this page already has a backlink
- The backlink is with the [[Feb 7th, 2022]]
- You can see this is also referenced in the "Linked Reference"
- If I want to reference a block within that page, ((62004d19-5803-4bae-9f90-322911262bf9))
- If I want to indent a block hit tab, and if I want to unindent a block, shift-tab
- If I want to draw something,
- [[draws/2022-02-07-11-40-42.excalidraw]]
- This creates a big canvas
  id:: 62004e6e-5d3a-4925-ae3c-caa1094ab3e7
- < results in formatting commands
- All of these bullet points are blocks
- Create a new page from here again, [[Taking Notes]]
  id:: 62004f0d-637f-4d28-a27f-5e964e5aa81c
-